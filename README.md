MODIFY

# Scheduler

A scheduler service 

# Installation

## Service App

This is an Exress server run via node that serves both the static files and the backend service app.

* install local dependencies

  ```
  cd server
  npm install
  cd ..
  ```

## Client App

This is the web-based front-end to this service. It's web stack but uses node for tools.

* install local dependencies

  ```
  cd client
  npm install
  cd ..
  ```

# Build/Configure

## Client app

* `grunt build`

## Server

* Edit server/config.js

# Running

`node server/server.js`

