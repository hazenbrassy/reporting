// first, parse the command line options
var opti = require('optimist'),
  argv = opti.usage('Usage: $0 --env=[short string]')
  .alias('e', 'env')
  .alias('h', 'help')['default']({env: process.env.NODE_ENV || 'dev'})
  .argv;

if (argv.help) {
  console.log(opti.help());
  return;
}

var express = require('express'),
  qs = require('qs'),
  app = express(),
  https = require('https'),
  http = require('http'),
  fs = require('fs'),
  path = require('path'),
  config = require('./config')(argv.env),
  accounting = require('./accounting'),
  maxAge = 0,
  home = process.env.HOME;

if (argv.env === 'prod')
  maxAge = (1000 * 60 * 60 * 24);

function Future(myAllComplete) {
  this.completions = 0;
  this.errString = undefined;
  this.errCode = undefined;
  this.data = {};
  this.allComplete = myAllComplete;
  return this
}

Future.prototype.expect = function(key) {
  this.data[key] = '';  
}

Future.prototype.err = function(errString, errCode) {
  this.errString = errString;
  this.errCode = errCode;
  if (this.allComplete) {
    this.allComplete(undefined, this.errString, this.errCode);
  }
}

Future.prototype.complete = function(key, value) {  
  this.completions++;
  this.data[key] = value;
  if (this.allComplete && !this.errString && this.completions >= Object.keys(this.data).length) {
    this.allComplete(this.data, undefined, undefined);
  } 
}


/* SSL config
var options = {
  key : fs.readFileSync(config.general.key).toString(),
  cert : fs.readFileSync(config.general.cert).toString(),
  ca: fs.readFileSync(config.general.ca).toString()
};
*/
// environment-specific logic goes here
app.configure('prod', function() {
});

// logging
app.use(express.logger(':remote-addr - - [:date] ":method :url HTTP/:http-version" :status :res[content-length] ":referrer" ":user-agent" :response-time'));

app.use(express.bodyParser());
app.use(express.compress());

// make the music w/ your mouth Biz!
//require('./lib/app-v1.js')(app); (CHECK OUT THIS FILE)

// handle static files
app.use(express['static'](path.resolve(__dirname, '../client/dist'), {maxAge: maxAge}));
console.log(path.resolve(__dirname, '../client/dist'));

// health-check
app.get('/health', function(req, res) {
  res.send(200, 'Server Running');
});

//JESSICA - reporting 
//this returns data in a JSON obj for the reporting overview report
app.get('/reportingOverview', function(req, res) {
  //open mySQL connection
  var mysql = require('mysql');
  var connection = mysql.createConnection ({
    host: 'localhost',
    port: '8889',
    database: 'TransitSherpa_TriMet',
    user:'root',
    password:'root',
  });

//future is complete - print out JSON
  var future = new Future(function() {
     res.header("Access-Control-Allow-Origin", "*");
    res.json(this.data);
  });

  future.expect("fee");
  future.expect("revenue");
  accounting.overview(future,connection);
  connection.end();
});

//this returns data in a JSON obj for the payments overview report
app.get('/payments', function(req, res) {
  //open mySQL connection

  var mysql = require('mysql');
  var connection = mysql.createConnection ({
    host: 'localhost',
    port: '8889',
    database: 'TransitSherpa_TriMet',
    user:'root',
    password:'root',
  });

//future is complete - print out JSON
  var future = new Future(function() {
     res.header("Access-Control-Allow-Origin", "*");
    res.json(this.data);
  });

  future.expect("batchRevenue");
  accounting.payments(future,connection);
  connection.end();
});

//this returns data in a JSON obj batch payment details
app.get('/batch', function(req, res) {
  //open mySQL connection
  var mysql = require('mysql');
  var connection = mysql.createConnection ({
    host: 'localhost',
    port: '8889',
    database: 'TransitSherpa_TriMet',
    user:'root',
    password:'root',
  });

//When future is complete - print out JSON
  var future = new Future(function() {
    res.header("Access-Control-Allow-Origin", "*");
    res.json(this.data);
    connection.end();
  });

//this gets the batch IDa of the latest batch for use in all the other queries 
  var futureBIDs = new Future(function() {
    accounting.batchData(future,this,connection);
  });
  
  accounting.selectBatchIDs(futureBIDs,connection);
  futureBIDs.expect("selectBatchIDs");

  

  future.expect("batchValueTotal");
  future.expect("batchSold");
//  future.expect("batchUnsold"); this is calculated because in the open batch this flag has not been set
  future.expect("batchCredited");
  future.expect("vestaFee");
});

/* configuration
var file = home + '/etc/config/scheduler_' + argv.env + '.toml';

var toml = require('toml-parser'),
  str = require('fs').readFileSync(file, 'utf-8'),
  configJson = JSON.stringify(toml(str), null, 2);

app.get('/config', function(req, res) {
  var query = qs.parse(req.query),
    data;

  if (query.callback) {
    data = query.callback + '(' + configJson + ');';
  } else if (query['var']) {
    data = 'var ' + query['var'] + ' = ' + configJson + ';';
  } else {
    data = configJson;
  }

  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET');
  res.header('Access-Control-Allow-Headers', 'Content-Type');

  res.end(data);
});
*/
// if all else fails
app.use(function(req, res, next){
  res.send(404, 'Page not found');
});

/*
https.createServer(options, app).listen(config.server.securePort, function() {
  console.log("Express secure server started with");
  console.log("Port: %s", config.server.securePort);
});
*/
http.createServer(app).listen(config.server.listenPort, function() {
  console.log("Express server started with");
  console.log("Port: %s", config.server.listenPort);
});
console.log("Env: %s", argv.env);
console.log("MaxAge: %s", maxAge);

