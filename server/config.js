var path = require('path'),
  fs = require('fs'),
  home = process.env['HOME'];


module.exports = function(env) {
  return {
    general: {
      key: path.resolve(home, 'etc/certs/star.transitsherpa.com.key'),
      cert: path.resolve(home, 'etc/certs/star.transitsherpa.com.crt'),
      ca: path.resolve(home, 'etc/certs/COMODOHigh-AssuranceSecureServerCA.crt')
    },
    server: {
      listenPort: 3030,
      securePort: 8443,
      distFolder: path.resolve(__dirname, '../client/dist')
    },
    db: {
      host: 'localhost',
      db: 'test'
    }
  };
};

