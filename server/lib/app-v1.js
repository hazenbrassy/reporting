var nodeSchedule = require('./node-schedule.js'),
  db = require('./db'),
  _ = require('underscore'),
  request = require('request'),
  Job = db.Job,
  basePath = '/api/v1/job',
  scheduled = {};

// on startup load in all jobs
Job.find(function(err, jobs) {
  if (err) {
    // BEER: handle error
    return;
  }
  scheduleJobs(jobs);
});

function scheduleJobs(jobs) {
  jobs.forEach(function(job) {
    scheduleJob(job);
  });
}

function scheduleJob(job) {
  console.log(job);
  var sched = new nodeSchedule.Job(job._id, function() {
    // BEER: use method to make right call
    request.get(job.value, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        console.log(body);
      }
    })
  });

  if (typeof job.schedule === 'number') {
    var date = new Date(job.schedule);
    sched.schedule(date);
  } else {
    var c = job.schedule,
      cronVals = [];
    cronVals.push(c.minute);
    cronVals.push(c.hour);
    cronVals.push(c.day);
    cronVals.push(c.month);
    cronVals.push(c.dayOfWeek);
    var spec = cronVals.join(' ');

    sched.schedule(spec);
  }

  // keep refs to currently scheduled jobs
  scheduled[job._id] = sched;
  console.log('new job: ' + job._id + ': ' + job.schedule);
}

function unscheduleJob(job) {
  scheduled[job._id].cancel();
}

function createJob(obj) {
  return new Job({
    name: obj.name,
    method: obj.method,
    value: obj.value,
    params: obj.parameters,
    schedule: obj.schedule,
    disabled: obj.disabled
  });
}

function updateJob(oldJob, newJob) {
  return _.extend(oldJob, {
    name: newJob.name,
    method: newJob.method,
    value: newJob.value,
    params: newJob.params,
    schedule: newJob.schedule,
    disabled: newJob.disabled
  });
}
/**
 * expose the API
 * @param app
 */
module.exports = function(app) {
  // create
  app.post(basePath, function(req, res) {
    var job = createJob(req.body);
    job.save(function(err) {
      if (err) {
        console.log('Error saving job');
      }

      scheduleJob(job);
    });
    res.json(job);
  });

  // read
  app.get(basePath + '/:id?', function(req, res) {
    if (req.params.id) {
      Job.findById(req.params.id, function(err, job) {
        res.json(job);
      });
      return;
    }

    Job.find(function(err, jobs) {
      if (err)
        res.end('Error: ' + err);

      res.json(jobs);
    });
  });

  // update
  app.put(basePath + '/:id', function(req, res) {
    Job.findById(req.params.id, function(err, job) {
      var job = updateJob(job, req.body);
      job.save(function(err) {
        if (err) {
          console.log('Error saving job');
        }
        unscheduleJob(job);
        scheduleJob(job);
      });
      console.log(job);
      res.json(job);
    });
  });

  // delete
  app['delete'](basePath + '/:id', function(req, res) {
    var id = req.params.id;

    Job.findByIdAndRemove(id, function(err, result) {
      if (err) {
        req.end(500, 'There was an error');
      }
      console.log('deleted ' + id);
      unscheduleJob(job);
      res.end();
    });
  });

};
