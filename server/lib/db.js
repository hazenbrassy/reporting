
var mongoose = require('mongoose'),
  config = require('../config')(),
  db = mongoose.connect('mongodb://' + config.db.host + '/' + config.db.db);

var asc = 1,
  desc = -1;

var JobSchema = new mongoose.Schema({
  // id is auto set
  name: String,
  method: String, // get|post|put|delete|head
  value: String, // usuall a URL for an API
  params: {}, // Schema.Types.Mixed; this object is passed to function
  schedule: {}, // [Date/schedule.RecurrenceRule/String]
  lastRunTime: Date,
  lastRunStatus: Number,
  disabled: Boolean
});

// indexes for listing jobs
JobSchema.index({
  lastRunTime: asc,
  name: desc
});
// show all jobs in order of next go by getting Date job types and sorting

var Job = mongoose.model('Job', JobSchema);


module.exports = {
  Job: Job
};
