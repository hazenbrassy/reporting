//SQL Calls
function overview(future,connection) {
	var queryString = "SELECT SUM(CPT.amount) AS REVENUE ";
	queryString += "FROM commerce_payment_transaction CPT,ts_payment_processor_transaction PPT "; 
	queryString += "WHERE CPT.status = \'success\' AND "; 
	queryString += "CPT.remote_id = PPT.payment_id";
//  console.log("Query: " + queryString);

	connection.query(queryString, function(err, rows, fields) {
	  if (err) throw err;
		  revenueTotal = rows[0].REVENUE;
	   	   future.complete("revenue",revenueTotal);
	});

	var queryString2 = "SELECT SUM(PPT.transaction_fee) AS FEE ";
	queryString2 += "FROM commerce_payment_transaction CPT,ts_payment_processor_transaction PPT "; 
	queryString2 += "WHERE CPT.status = \'success\' AND "; 
	queryString2 += "CPT.remote_id = PPT.payment_id ";
//  console.log("Query: " + queryString2);

	connection.query(queryString2, function(err, rows, fields) {
	  if (err) throw err;
		  var feeTotal = rows[0].FEE;
	   	   future.complete("fee",feeTotal);

	});
}

function payments(future,connection) {
	var queryString =  "SELECT SUM(TRANSACTIONS.AMOUNT) AMOUNT, BATCHES.BID, BATCHES.CREATED, BATCHES.active " ;
	queryString += "FROM ";
	queryString += "(SELECT DISTINCT CPT.amount, INV.bid, CPT.remote_id ";
    queryString += "FROM ts_inventory INV,ts_ticket TICK,commerce_payment_transaction CPT ";
	queryString +=	"WHERE INV.TS_ID = TICK.TS_ID ";
	queryString +=	"AND CPT.order_id = TICK.order_id)  AS TRANSACTIONS, ts_inventory_batch BATCHES ";
	queryString +=	"WHERE TRANSACTIONS.bid =  BATCHES.bid ";
	queryString +=  "GROUP BY TRANSACTIONS.BID " ;
	queryString += "ORDER BY CREATED DESC ";
	queryString += "LIMIT 0,10"; 

//  console.log("Query: " + queryString);

	connection.query(queryString, function(err, rows, fields) {
	  if (err) throw err;
		   batchRevenue = JSON.stringify(rows);
	   	   future.complete("batchRevenue",batchRevenue);

	});
}

function selectBatchIDs(futureBIDs,connection) {
	//Get the id of the open batch - this is used in all the other queries
	var queryString =  "SELECT bid ";
	queryString += "FROM ts_inventory_batch BATCH ";
//	queryString += "WHERE BATCH.active=1";
	connection.query(queryString, function(err, rows, fields) {
	  if (err) throw err;
				var str = "";
				rows.forEach(
				function makeCSVString(value) {
					str += value.bid + ",";
					csvString = str.substring(0, str.length - 1); //cuts off last extra comma
				}
			);
			console.log("csvString" + csvString);
		   futureBIDs.complete("selectBatchIDs",csvString);
	});
}

function batchData(future,futureBIDs,connection) {
//Originally this was just the open batch. Refactored to grab the latest 10(?) batches sorted by date
	selectBatchIDs = "(" + futureBIDs.data["selectBatchIDs"] + ") ";
	console.log("selectBatchIDs" + selectBatchIDs);
	

//this is total value of the batch
	var queryString =  "SELECT SUM(price) AS batchValue, bid ";
	queryString += "FROM ts_inventory ";
	queryString += "WHERE ts_inventory.bid IN " + selectBatchIDs + " " ;
	queryString += "GROUP BY ts_inventory.bid ";
	queryString += "ORDER BY ts_inventory.created ";
	queryString += "LIMIT 0,10"; 
	console.log("Query:" + queryString);

//This is tickets SOLD (does not include comps)
	var queryString2 =  "SELECT SUM(price) AS batchSold,bid ";
	queryString2 += "FROM ts_inventory, ts_ticket ";
	queryString2 += "WHERE ts_ticket.ts_id = ts_inventory.ts_id ";
	queryString2 += "AND  ts_ticket.credited_ticket = 0 ";
	queryString2 += "AND ts_inventory.bid IN " + selectBatchIDs + " " ;
	queryString2 += "GROUP BY ts_inventory.bid ";
	queryString2 += "ORDER BY ts_inventory.created ";
	queryString2 += "LIMIT 0,10"; 
	console.log("Query2:" + queryString2);


//This is tickets Credited (comped)
	var queryString3 =  "SELECT SUM(price) AS batchCredited, bid ";
	queryString3 += "FROM ts_inventory, ts_ticket ";
	queryString3 += "WHERE ts_ticket.ts_id = ts_inventory.ts_id ";
	queryString3 += "AND ts_ticket.credited_ticket = 1 ";
	queryString3 += "AND ts_inventory.bid IN " + selectBatchIDs + " " ;
	queryString3 += "GROUP BY ts_inventory.bid ";
	queryString3 += "ORDER BY ts_inventory.created ";
	queryString3 += "LIMIT 0,10"; 
	console.log("Query3:" + queryString3);

//This is the vesta fees charged - THIS IS A HORRIBLE FIVE TABLE JOIN
/*	var queryString4 =  "SELECT SUM(TRANSACTIONS.FEE) FEE, BATCHES.BID, BATCHES.CREATED, BATCHES.active " ;
	queryString4 += "FROM ";
	queryString4 += "(SELECT DISTINCT CPT.fee, INV.bid, CPT.remote_id ";
    queryString4 += "FROM ts_inventory INV,ts_ticket TICK,commerce_payment_transaction CPT ";
	queryString4 +=	"WHERE INV.TS_ID = TICK.TS_ID ";
	queryString4 +=	"AND CPT.order_id = TICK.order_id)  AS TRANSACTIONS, ts_inventory_batch BATCHES ";
	queryString4 +=	"WHERE TRANSACTIONS.ts_inventory.bid IN " + selectBatchIDs + " ";
	queryString4 +=  "GROUP BY TRANSACTIONS.BID " ;
	queryString4 += "ORDER BY CREATED DESC ";
	queryString4 += "LIMIT 0,10"; 
	console.log("Query4:" + queryString4);
*/
	connection.query(queryString, function(err, rows, fields) {
	  if (err) throw err;
		   future.complete("batchValueTotal",JSON.stringify(rows));
	});
	connection.query(queryString2, function(err, rows, fields) {
	  if (err) throw err;
		   future.complete("batchSold",JSON.stringify(rows));
	});
	connection.query(queryString3, function(err, rows, fields) {
	  if (err) throw err;
		   future.complete("batchCredited",JSON.stringify(rows));

		   //JUST FAKING VESTA FEE FOR NOW	
		   future.complete("vestaFee",JSON.stringify(rows));
	});

//	connection.query(queryString4, function(err, rows, fields) {
//	  if (err) throw err;
//		   var vestaFee = (rows[0].Fee == null) ? "0" : rows[0].Fee;
//		   console.log("vestaFee:" + vestaFee);
//		   future.complete("vestaFee",vestaFee);
//	});
}

module.exports = {
	overview: overview,
	payments: payments,
	selectBatchIDs: selectBatchIDs,
	batchData: batchData
}


