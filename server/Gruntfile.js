module.exports = function(grunt) {

  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    jshint: {
      files: [
        'Gruntfile.js', 
        'server.js',
        'lib/**/*.js',
        '!lib/node-schedule*',
        'test/**/*.js'
      ],
      jshintrc: '../.jshintrc'
    },
    watch: {
      files: ['<%= jshint.files %>'],
      tasks: ['jshint']
    }
  });

  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');

  grunt.registerTask('default', ['jshint']);
  grunt.registerTask('work', ['watch', 'supervise']);

  grunt.registerTask('supervise', function() {
    this.async();
    require('supervisor').run(['server.js']);
  });

};
