module.exports = function(grunt) {

  grunt.initConfig({
    distdir: 'dist',
    pkg: grunt.file.readJSON('package.json'),
    concat: {
      options: {
        separator: ';'
      },
      vendor: {
        src: [
          'vendor/jquery-2.0.0.min.js', 
          'vendor/underscore-min.js', 
          'vendor/angular.min.js', 
          'vendor/moment-min.js', 
          'vendor/sjcl.js',
          'vendor/custom.modernizr.js',
          'vendor/foundation.min.js',
          'vendor/jquery-ui-1.10.2.custom.min.js'
        ],
        dest: '<%= distdir %>/assets/js/vendor.min.js'
      },
      code: {
        src: [
          'src/app/js-modules/src/main/main.js',
          'src/app/config.js',
          'src/app/js-modules/src/utility/utility.js',
          'src/app/js-modules/src/persistence/localstorage.js',
          'src/app/app.js', 
          'src/app/routes.js',
          'src/app/controllers.js',
          'src/app/services.js',
          'src/app/directives.js'
        ],
        dest: '<%= distdir %>/assets/js/code.js'
      },
      css: {
        src: [
          'src/assets/css/*.css'
        ],
        dest: '<%= distdir %>/assets/css/all.css'
      }
    },
    clean: ['<%= distdir %>/*'],
    copy: {
      assets: {
        files: [
          { dest: '<%= distdir %>/', src : '**/*.html', expand: true, cwd: 'src/' }
          // { dest: '<%= distdir %>/assets/css/', src : '**/*.css', expand: true, cwd: 'src/assets/css/' }
        ]
      }
    },
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
      },
      dist: {
        files: {
          '<%= distdir %>/assets/js/code.min.js': ['<%= concat.code.src %>']
        }
      }
    },
    jshint: {
      files: [
        'Gruntfile.js', 
        'src/**/*.js', 
        '!src/app/js-modules/**/*.js',
        'test/**/*.js'
      ],
      jshintrc: '../.jshintrc'
    },
    sed: {
      hidelog: {
        src: ['src/*.js', 'src/account-app/**/*.js', 'src/service-module/*.js'],
        pattern: /[^!]{2} console.log/g,
        replacement: '   // console.log'
      },
      showlog: {
        src: ['src/*.js', 'src/account-app/**/*.js', 'src/service-module/*.js'],
        pattern: /\/\/ console.log/g,
        replacement: 'console.log'
      }
    },
    watch: {
      scripts: {
        files: ['Gruntfile.js', 'src/**/*.js'],
        tasks: ['copy', 'concat', 'uglify']
      },
      assets: {
        files: ['src/**/*.html', 'src/**/*.css'],
        tasks: ['copy', 'concat:css']
      }
    },
    connect: {
      server: {
        options: {
          port: 9001,
          base: 'www-root'
        }
      }
    },
    mocha: {
      // all: [ 'test/**/!(test2).html' ],
      other: {
        src: [ 'test/**/!(index).html' ],
        options: {
          run: true
        }
      }
    },
    karma: {
      e2e: {
        configFile: 'test/karma.e2e.conf.js',
        // runnerPort: 9999,
        singleRun: true, // doing like unit right now
        browsers: ['PhantomJS']
        // browsers: ['PhantomJS', 'Chrome']
      },
      unit: {
        configFile: 'test/karma.unit.conf.js',
        singleRun: true,
        browsers: ['PhantomJS']
      }
    },

    // working mostly but hard to do anything w/o jquery/underscore and
    // only including one file at a time
    simplemocha: {
      options: {
        ui: 'bdd',
        reporter: 'dot',
        timeout: 5000
      },
      unit: {
        src: [
          'src/main/main.js',
          'src/ticket/ticket.js',
          'test/unit/**/*.js'
        ]
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-connect');
  grunt.loadNpmTasks('grunt-mocha');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-sed');
  grunt.loadNpmTasks('grunt-karma');

  // NOTE: npm install phantomjs -g to work w/ PhantomJS

  grunt.registerTask('test', ['jshint']);
  grunt.registerTask('build', ['clean', 'copy', 'concat', 'uglify']);

  grunt.registerTask('default', ['jshint', 'copy', 'uglify']);

};
