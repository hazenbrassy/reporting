var vestatoken = vestatoken ? vestatoken : {};

vestatoken.init = function(request_params)
{
	vestatoken.ServiceURL = request_params.ServiceURL;
	vestatoken.AccountName = request_params.AccountName;
}
										  
										  
vestatoken._sendMessage = function(request_params)
{
   var payload         = request_params.payload         ? request_params.payload         : {};
   var onSuccess       = request_params.onSuccess       ? request_params.onSuccess       : function(){};
   var onFailed        = request_params.onFailed        ? request_params.onFailed        : function(){};
   var onInvalidInput  = request_params.onInvalidInput  ? request_params.onInvalidInput  : function(){};

   var url             = vestatoken.ServiceURL + request_params.message;

   payload['AccountName'] = vestatoken.AccountName;

   var jqxhr = $.ajax({
          type         : "GET"
        , url          : url
        , dataType     : "jsonp"
        , data         : payload
        , processData  : true
        , beforeSend   : function(jqxObj) {
                      if(jqxObj && jqxObj.overrideMimeType)
                        {
                        jqxObj.overrideMimeType("application/j-son;charset=UTF-8");
                        }
                     }
   });

   jqxhr.done(
         function(data){
            if(! data.ResponseCode)
            {
               onFailed('Unknown System Failure');
            }
            else if(data.ResponseCode >= 500)
            {
               onInvalidInput(data.ResponseText);
            }
            else if(data.ResponseCode != 0)
            {
               onFailed(data.ResponseText);
            }
            else
            {
               onSuccess(data);
            }
         });

   jqxhr.fail(
         function(jqXHR, textStatus) {
            onFailed(textStatus);
      });
}


vestatoken.validatecreditcard = function(request_params)
{

   var creditcardnumber    = request_params.ChargeAccountNumber;
   var chargecvn           = request_params.ChargeCVN;
   var chargeexpirationmmyy    = request_params.ChargeExpirationMMYY;
   var cardholderfirstname   = request_params.CardHolderFirstName;
   var cardholderlastname    = request_params.CardHolderLastName;
   var cardholderaddressline1    = request_params.CardHolderAddressLine1;
   var cardholderaddressline2    = request_params.CardHolderAddressLine2;
   var cardholdercity    = request_params.CardHolderCity;
   var cardholderregion    = "OR"; //"request_params.CardHolderRegion"OR;
   var cardholderpostalcode    = request_params.CardHolderPostalCode;
   var cardholdercountrycode = "US";
   var transactionid = 349; //TransactionID
   var chargesource= "WEB";
   var websessionid="102_425";
   var paymentdescriptor = "GLOBESHERPATRIMET";
   var password = "GLOBESHERPATRIMETTEST1";
//GLOBESHERPATRIMETTEST1

   //alert (cardholderregion);

  //&WebSessionID=111111112
  //&PaymentDescriptor=Early%20Test%20Order
							
   var userOnSuccess       = request_params.onSuccess ? request_params.onSuccess : function(){};
   var userOnFailed        = request_params.onSuccess ? request_params.onFailed  : function(){};
   var userOnInvalidInput  = request_params.onSuccess ? request_params.onInvalidInput  : function(){};

   //userOnInvalidInput(cardholderregion);
     // return;

   if ( ! vestatoken._isNumeric(creditcardnumber) )
   {
      userOnInvalidInput('Charge Account Number must be numeric.');
      return;
   }

   if ( creditcardnumber.length < 13 )
   {
      userOnInvalidInput('Charge Account Number must be at least 13 digits.');
      return;
   }

   if ( ! vestatoken._isNumeric(creditcardnumber) )
   {
      userOnInvalidInput('Charge Account Number is not valid. Please verify.');
      return;
   }

   vestatoken._sendMessage(
   { message        : 'ValidateChargeAccount'
   , payload        : {TransactionID: transactionid, AccountName:paymentdescriptor ,Password: password, CardHolderAddressLine1 : cardholderaddressline1, CardHolderAddressLine2 : cardholderaddressline2
	,  CardHolderCity:cardholdercity, CardHolderFirstName:cardholderfirstname ,CardHolderLastName: cardholderlastname
	,ChargeAccountNumber : creditcardnumber,ChargeCVN : chargecvn, ChargeExpirationMMYY: chargeexpirationmmyy, 
	CardHolderPostalCode : cardholderpostalcode, CardHolderRegion:cardholderregion, CardHolderCountryCode: cardholdercountrycode, ChargeSource: chargesource, WebSessionID:websessionid, PaymentDescriptor: paymentdescriptor
	 }
   , onSuccess      : userOnSuccess
   , onFailed       : userOnFailed
   , onInvalidInput : userOnInvalidInput
   });
}


vestatoken.getpermcreditcardtoken = function(request_params)
{
	var account = "GLOBESHERPATRIMET";
   var password = "GLOBESHERPATRIMETTEST1";
   var creditcardnumber    = request_params.ChargeAccountNumber;
   var userOnSuccess       = request_params.onSuccess ? request_params.onSuccess : function(){};
   var userOnFailed        = request_params.onSuccess ? request_params.onFailed  : function(){};
   var userOnInvalidInput  = request_params.onSuccess ? request_params.onInvalidInput  : function(){};

   if ( ! vestatoken._isNumeric(creditcardnumber) )
   {
      userOnInvalidInput('Charge Account Number must be numeric.');
      return;
   }

   if ( creditcardnumber.length < 13 )
   {
      userOnInvalidInput('Charge Account Number must be at least 13 digits.');
      return;
   }

   if ( ! vestatoken._isNumeric(creditcardnumber) )
   {
      userOnInvalidInput('Charge Account Number is not valid. Please verify.');
      return;
   }
//
   vestatoken._sendMessage(
   { message        : 'ChargeAccountToPermanentToken'
   , payload        : {ChargeAccountNumber : creditcardnumber,  AccountName:account, Password: password }
   , onSuccess      : userOnSuccess
   , onFailed       : userOnFailed
   , onInvalidInput : userOnInvalidInput
   });
}
//Should be in a seperate file
vestatoken.getcreditcardtoken = function(request_params)
{
   var creditcardnumber    = request_params.ChargeAccountNumber;
   var userOnSuccess       = request_params.onSuccess ? request_params.onSuccess : function(){};
   var userOnFailed        = request_params.onSuccess ? request_params.onFailed  : function(){};
   var userOnInvalidInput  = request_params.onSuccess ? request_params.onInvalidInput  : function(){};

   if ( ! vestatoken._isNumeric(creditcardnumber) )
   {
      userOnInvalidInput('Charge Account Number must be numeric.');
      return;
   }

   if ( creditcardnumber.length < 13 )
   {
      userOnInvalidInput('Charge Account Number must be at least 13 digits.');
      return;
   }

   if ( ! vestatoken._isNumeric(creditcardnumber) )
   {
      userOnInvalidInput('Charge Account Number is not valid. Please verify.');
      return;
   }

   vestatoken._sendMessage(
   { message        : 'ChargeAccountToTemporaryToken'
   , payload        : {ChargeAccountNumber : creditcardnumber}
   , onSuccess      : userOnSuccess
   , onFailed       : userOnFailed
   , onInvalidInput : userOnInvalidInput
   });
}

vestatoken._isNumeric = function(value)
{
  if (value == null || !value.toString().match(/^[-]?\d*\.?\d*$/)) return false;
  return true;
}

vestatoken._isMod10 = function(val)
{
   var iTotal = 0;
   var parity = val.length % 2;

   for (var i=0; i<val.length; i++)
   {
      var calc = parseInt(val.charAt(i));
      if (i % 2 == parity) calc = calc * 2;
      if (calc > 9)        calc = calc - 9;
      iTotal += calc;
   }

   if ((iTotal%10)==0) return true;

   return false;
}
