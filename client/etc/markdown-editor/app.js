var markdownEditor = angular.module('markdown-editor', []);

markdownEditor.directive('markdown', function() {
  var showdown = new Showdown.converter(),
      template = 
        '<section>' +
        '<textarea ng-model="content" cols="80" rows=10"></textarea>' +
        '<article ng-bind-html-unsafe="markdown(content)"></article>' +
        '</section>';
  
  return {
      restrict: 'E',
      scope: {},
      compile: function(templateElement) {
          var initialContent = templateElement.html(),
            key = 'GS-Temp-MD';
          templateElement.html(template);
          return function(scope, element, attrs) {
            scope.content = JSON.parse(window.localStorage.getItem(key)) || initialContent;
            scope.markdown = showdown.makeHtml;
            element.on('keyup', function(e) {
              window.localStorage.setItem(key, JSON.stringify(scope.content));
            });
          }
      }
  }
});

