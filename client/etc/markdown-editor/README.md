Markdown Editor
===============

A simple web-based form for WYSIWYG creation of markdown content which is the format used for README's across most projects.

Bitbucket and github know to parse README.md files and print them pretty.

Just load the index.html file into your browser after downloading this git repo and edit away.

