// Angular Controllers
//
;
(function() {

  angular.module('scheduler-service').controller('HomeCtrl', ['$scope', '$location', function HomeCtrl($scope, $location) {
    $scope.demo = function() {
      $location.url('/demo/');
    };

  }]);

  angular.module('scheduler-service').controller('SidebarCtrl', ['$scope', 'Utils', '$location', 'APIService', function SidebarCtrl($scope, Utils, $location, APIService) {
    $scope.templates = {
      list: 'partials/list.html'
    };
    $scope.template = $scope.templates.list;
    $scope.list = [];

    getJobs();

    /*
     $scope.list = _.map(JobManager.getPersistedJobs(), function(item) {
     item.params = Utils.serialize(item.params);
     return item;
     });
     */

    $scope.$on('Job:update', function() {
      getJobs();
    });

    $scope.viewJob = function(job) {
      $location.url('/demo/' + job._id);
    };

    // BEER: abstract out into library
    function getJobs() {
      APIService.getJobs()
        .success(function(result) {
          $scope.list = _.map(result, function(item) {
            item.parameters = Utils.serialize(item.params);
            return item;
          });
        });
    }


  }]);

  angular.module('scheduler-service').controller('DemoCtrl', ['$scope', '$rootScope', '$location', 'APIService', 'JobManager', '$routeParams', '$log', function HomeCtrl($scope, $rootScope, $location, APIService, JobManager, $routeParams, $log) {
    $scope.id = $routeParams.id;

    $scope.createJob = function() {
      $location.url('/demo/');
    };

    $scope.methods = [
      {label: 'XHR:POST', value: 'post'},
      {label: 'XHR:PUT', value: 'put'},
      {label: 'XHR:GET', value: 'get'},
      {label: 'XHR:DELETE', value: 'delete'},
      {label: 'XHR:HEAD', value: 'head'}
    ];

    $scope.Job = {
      method: $scope.methods[0],
      params: [
        {name: '', value: ''}
      ]
    };

    // BEER: move somewhere else
    $scope.minutePattern = /([0-59]|\*)/;
    $scope.hourPattern = /([0-23]|\*)/;
    $scope.dayOfMonthPattern = /([1-31]|\*)/;
    $scope.monthPattern = /([1-12]|\*)/;
    $scope.dayOfWeekPattern = /([0-7]|\*)/;


    if ($scope.id) {
      APIService.getJob($scope.id)
        .success(function(result) {
          $scope.Job = result;

          if ($scope.Job.schedule) {
            if (typeof $scope.Job.schedule === 'number') {
              $scope.scheduleType = 'date';
              var datetime = moment($scope.Job.schedule);
              $scope.date = datetime.format('MM/DD/YYYY');
              $scope.Date = {
                hour: parseInt(datetime.format('HH')),
                minute: parseInt(datetime.format('mm')),
                second: parseInt(datetime.format('ss'))
              };
            } else {
              $scope.scheduleType = 'cron';
              $scope.Cron = $scope.Job.schedule;
            }
          }
        }
      )
      ;
    }

    $scope.addParam = function() {
      if (!$scope.Job.params) {
        $scope.Job.params = [];
      }
      $scope.Job.params.push({});
      ;
    };
    $scope.removeParams = function(index) {
      $scope.Job.params.splice(index, 1);
    };


    // save a Job
    $scope.save = function() {
      $scope.Job.schedule = ($scope.scheduleType === 'date') ? buildDate() : $scope.Cron;

      APIService.createJob($scope.Job)
        .success(function(result) {
          // save a copy
          // JobManager.setJob(result);
          $rootScope.$broadcast('Job:update');
          $location.url('/demo/');
        });
    };

    // update a Job
    $scope.update = function() {
      $scope.Job.schedule = ($scope.scheduleType === 'date') ? buildDate() : $scope.Cron;

$log.log('E', $scope.Job);
      APIService.updateJob($scope.Job)
        .success(function(result) {
          $rootScope.$broadcast('Job:update');
          $location.url('/demo/');
        });
    };

    // remove a job
    // BEER: these 3 can be abstracted out to keep DRY
    $scope.delete = function(job) {
      if (confirm('Are you sure?')) {
        APIService.removeJob(job)
          .success(function(result) {
            $rootScope.$broadcast('Job:update');
            $location.url('/demo/');
          });
      }
    };

    // date picker stuff
    $scope.datepickerOpts = {
      autoclose: true,
      format: 'mm/dd/yyyy'
    };

    // defaults
    $scope.Date = {
      hour: 0,
      minute: 0,
      second: 0
    };

    function buildDate() {
      var time = moment($scope.date, 'MM/DD/YYYY').valueOf();
      time += $scope.Date.minute * 60 * 1000;
      time += $scope.Date.hour * 60 * 60 * 1000;
      time += $scope.Date.second * 1000;
      return time;
    }

  }]);


}());

