/**!
 *
 */
;(function() {
  // "use strict";

  GlobeSherpa.service({
    name: 'SherpaConfig',
    dependencies: [],
    extension: GlobeSherpa.Manager,
    factory: function() {

      /*
       This should be set up via a call to /config?var=SherpaConfig
       */
      if (typeof SherpaConfig === 'undefined') {
        console.log('WARNING: CONFIG IS NOT SET');
        // setting in global namespace
        SherpaConfig = {
          "global": {
            "env": "local"
          },
          "api": {
            "timeout": "20000"
          },
          "servers": {
            "logger": "https://localhost:8443",
            "api": "https://localhost:8443"
          }
        };
      }

      return SherpaConfig;
    }
  });

}());
