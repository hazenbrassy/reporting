(function() {

  angular.module('scheduler-service')

    .constant('SherpaConfig', GlobeSherpa.inject('SherpaConfig'))

    .factory('Utils', function() {
      return GlobeSherpa.inject('Utility');
    })
    .factory('Persistence', function() {
      return GlobeSherpa.inject('Persistence');
    })

    .factory('JobManager', ['Persistence', function(Persistence) {
      var namespace = 'job-';
      return {
        fetchJobs: function() {
          // grab jobs from remote data store
        },
        getPersistedJobs: function() {
          var all = Persistence.getAll();
          return _.chain(all)
            .filter(function(item, key) {
              return key.indexOf(namespace) !== -1;
            })
            .map(function(job) {
              return JSON.parse(job);
            })
            .value();
        },
        getJob: function(id) {
          // get by id; probs use Persistence
          return Persistence.get(namespace + id);
        },
        setJob: function(job) {
          Persistence.set(namespace + job._id, job);
        }
      };
    }])

    .factory('APIService', ['$http', 'SherpaConfig', function($http, SherpaConfig) {
      var basePath = '/api/v1/job',
        server = SherpaConfig.servers.api,
        uri = server + basePath;

      return {
        getJobs: function() {
          return $http.get(uri);
        },
        getJob: function(id) {
          return $http.get(uri + '/' + id);
        },
        createJob: function(job) {
          return $http.post(uri, job);
        },
        updateJob: function(job) {
          return $http.put(uri + '/' + job._id, job);
        },
        removeJob: function(job) {
          return $http.delete(uri + '/' + job._id);
        }
      }
    }])


  /**
   *
   *
   Utils.logError({
        severity: level,
        type: label,
        msg: errorString
      });
   */
    .factory('exceptionHandlerFactory', ['SherpaConfig', 'Utils', function(SherpaConfig, Utils) {

      return function(exception, cause) {
        // BEER: functional program this combinatorial/partial w/ below
        if (SherpaConfig.global.env === 'test' || SherpaConfig.global.env === 'dev' || SherpaConfig.global.env === 'local') {
          alert('Error: see console');
          window.console.log('Error', Utils.formatError(exception), cause);
        }

        if (SherpaConfig.global.env !== 'prod') {
          Utils.logError('WARN', exception + '; ' + cause);
        }

        return;
      };

    }])
    .config(['$provide', function($provide) {
      $provide.decorator('$exceptionHandler', ['exceptionHandlerFactory', function(exceptionHandlerFactory) {
        return exceptionHandlerFactory;
      }]);
    }]);

}());
