;(function() {

  angular.module('scheduler-service')
    .config(['$routeProvider', function($routeProvider) {
      $routeProvider.
        when('/home', {
          templateUrl: 'partials/home.html',
          controller: 'HomeCtrl'
        }).
        when('/demo/:id', {
          templateUrl: 'partials/demo.html',
          controller: 'DemoCtrl'
        }).
        otherwise({redirectTo: '/home'});
    }]);

})();
