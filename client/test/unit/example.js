var assert = chai.assert;

var mockJobs = [
  {
    "__v": 1,
    "_id": "51840ff6664a4ecb2d000001",
    "method": "XHR:POST",
    "name": "Date Job",
    "params": [
      {
        "$$hashKey": "00I",
        "name": "param1",
        "value": "value1"
      }
    ],
    "schedule": 1370028600000,
    "value": "https://localhost:8443/#/demo/"
  },
  {
    "__v": 1,
    "_id": "51841103d89fc5f92d000002",
    "method": "XHR:POST",
    "name": "Cron Job",
    "params": [
      {
        "name": "one",
        "value": "1"
      }
    ],
    "schedule": {
      "day": "3",
      "dayOfWeek": "5",
      "hour": "2",
      "minute": "1",
      "month": "4"
    },
    "value": "https://localhost:8443/#/demo/"
  }
];

describe('The APIService Class', function() {

  var scope,
    APIService,
    httpBackend,
    SherpaConfig,
    assert = chai.assert;

  beforeEach(function() {
    module('scheduler-service');

    inject(function($rootScope, $injector, $httpBackend) {
      scope = $rootScope.$new();
      APIService = $injector.get('APIService');
      httpBackend = $httpBackend;
      SherpaConfig = $injector.get('SherpaConfig');
    });
  });

  describe('fetch jobs', function() {

    it('should extend the options', function(done) {
      httpBackend.expectGET(SherpaConfig.servers.api + '/api/v1/job').respond(mockJobs); // BEER: this seems silly

      var promise = APIService.getJobs();

      assert(angular.isFunction(promise.then));

      promise.then(function(res) {
        assert.equal(res.data, mockJobs);
        done();
      });
      httpBackend.flush();
    });
  });

});
