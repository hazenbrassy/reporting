/**
 *
 * Needs a lot of love!
 */

var assert = chai.assert;

/*
describe('Buzz Client', function() {
  it('should filter results', function() {
    input('user').enter('jacksparrow');
    element(':button').click();
    expect(repeater('ul li').count()).toEqual(10);
    input('filterText').enter('Bees');
    expect(repeater('ul li').count()).toEqual(1);
  });
});
*/

describe('The APIService Class', function() {

  var scope, APIService, assert = chai.assert;

  beforeEach(function() {
    module('scheduler-service');

    inject(function($rootScope, $injector) {
      scope = $rootScope.$new();
      APIService = $injector.get('APIService');
    });
  });
  describe('fetch jobs', function() {

    it('should extend the options', function(done) {
      this.timeout(10000);

      APIService.getJobs().success(function(result) {
        assert.isObject(result);
        done();
      });
      // expect(APIService.options).toEqual({show: false, foo: 'bar'});
    });
  });

});
