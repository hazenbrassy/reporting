
basePath = '..';

// build order is important...
files = [
  MOCHA,
  MOCHA_ADAPTER,
  'node_modules/chai/chai.js',
  'dist/assets/js/vendor.min.js',
  'dist/assets/js/code.js',
  'test/vendor/*.js',
  'test/unit/*.js'
];

exclude = [
  'karma.conf.js'
];

colors = true;

// autoWatch = false;
// browsers = ['Chrome'];
// singleRun = true;

reporters = ['progress'];

