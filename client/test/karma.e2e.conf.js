
basePath = '..';

files = [
  MOCHA,
  MOCHA_ADAPTER,
  'node_modules/chai/chai.js',
  'dist/assets/js/vendor.min.js',
  'dist/assets/js/code.js',
  'test/e2e/*.js'
];

exclude = [
  'karma.conf.js'
];

colors = true;

// autoWatch = false;
// browsers = ['Chrome'];
// singleRun = true;

reporters = ['progress'];

